# LPaaS Server #

## Disclaimer ##
This is a graduation project by Andrea Muccioli based on the original research project [tuProlog](https://bitbucket.org/tuprologteam/tuprolog) of the University of Bologna - Alma Mater Studiorum. This repository is not the official one and its only purpose is to showcase the code written for said project.

## What is LPaaS Server ##
This is an application server prototype developed to put into practice the principles of the LPaaS (Logic Programming as a Service) model. The server wraps an inference engine realized using tuProlog, and exposes its functionalities to the clients via RESTful Web Services. The server has two different access interfaces (RESTful WS), one main interface used to ask for the solution(s) of goals from a set list and to check the engine configurations, the second one with restricted access, used to configure the server and the engine (set list of possible goals, set theory of the inference engine, etc...).

## Features ##
* RESTful WS access APIs
* Light-weight inference engine dynamically and statically configurable
* Authentication managed via [JWT](https://jwt.io/)
* Stateful interaction with session tokens
* Light-weight; working on low-performance devices such as Raspberry Pi.
* Random-generated data based on xml patterns for testing purposes.

## Application Scenario ##
This prototype has been put to the test in a simulation of a hypothetical Smart Bathroom; a Bathroom whose elements implements the developed server to shape their behaviour. Each bathroom element is simulated by an instance of the LPaaS server configured differently from case to case. Each element (toilet, toothbrush and a wearable device) gets raw data from its sensors (e.g. urine analysis values, toothbrush battery level, heart rate of the user) that updates the knowledge base of its own inference engine. A client can ask the server to solve different goals to get higher level data (e.g. fever -> tachycardy && high_temperature) that the server's inference engine generate based on the raw data currently loaded inside its Knowledge Base.
In this specific application scenario the client is an Android application that sends requests to each smart bathroom element, gets the results, uses them as input of its own inference engine where they are combined and elaborated, and finally outputs a high level, readable list of warnings for the user to read (e.g. "brush your teeth" "possible diabetes", "possible oral infection").
The idea is to show the potential of the application (and the LPaaS paradigm) in a scenario which could become reality in the near future.

Note: The data used, for testing purposes, does not come from real sensors, but is generated randomly every minute inside the server itself based on a xml configuration file.

![gitsmart toilet devices2.png](https://bitbucket.org/repo/nqqLan/images/118392777-gitsmart%20toilet%20devices2.png) ![gitlist1.png](https://bitbucket.org/repo/nqqLan/images/2656249600-gitlist1.png)

## Installation guide ##

* Download project code
* Build using [Maven](https://maven.apache.org/download.cgi). (Execute "mvn clean install" in the project main folder)
* Deploy .war generated file on [Payara](http://www.payara.fish/) or [Payara Micro](http://www.payara.fish/payara_micro)
* The main REST interface can be found at: "*/rest/tuProlog/main" and the configuration one at: "*/rest/tuProlog/configuration" (e.g http://localhost:8080/rest/tuProlog/main)

## Contacts ##

* Code by: [Andrea Muccioli](mailto:andre.muccio@gmail.com)
* Official tuProlog page @ APICe: http://apice.unibo.it/xwiki/bin/view/Tuprolog/WebHome